<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminDashboardController extends Controller
{
    public function dashboard()
    {
        $data['title'] = "Dashboard";

        $data['modul'] = array(
            array(
                "icon"  => "people",
                "title" => "Siswa",
                "url"   => url('admin/student'),
                "description"=> "Manajemen Siswa"
            ),
            array(
                "icon"  => "people",
                "title" => "Guru",
                "url"   => url('admin/teacher'),
                "description"=> "Manajemen Guru"
            ),
            array(
                "icon"  => "people",
                "title" => "Kelas",
                "url"   => url('admin/teacher'),
                "description"=> "Manajemen Guru"
            ),
            array(
                "icon"  => "watch_later",
                "title" => "Jadwal",
                "url"   => url('admin/teacher'),
                "description"=> "Manajemen Guru"
            ),
            array(
                "icon"  => "description",
                "title" => "Ujian",
                "url"   => url('admin/teacher'),
                "description"=> "Manajemen Guru"
            ),
            array(
                "icon"  => "business_center",
                "title" => "SPP",
                "url"   => url('admin/teacher'),
                "description"=> "Manajemen Pembayaran"
            )
        );

        return view('admin.dashboard.dashboard',$data);
    }
}
