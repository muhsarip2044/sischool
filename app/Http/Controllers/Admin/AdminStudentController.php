<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;


use Datatables;

use App\Student;

class AdminStudentController extends Controller
{
    public function index()
    {
        $data['title'] = "Data Siswa";
        $data['breadcrumb'] = array(
            url('/admin')    => "Beranda",
            ""          => "Data Siswa"
        );
        return view('admin.student.list_student',$data);
    }
    public function getDtRowData()
    {
        $students = Student::select(['nis','name','city']);

        return Datatables::of($students)
            ->addColumn('action', function ($student) {
                return '
                <a href="'.url('admin/student/edit/'.$student->nis).'" class="btn btn-xs btn-primary"><i class="material-icons">mode_edit</i> Edit</a>
                <button onclick="delete_data(\''.$student->nis.'\')" class="btn btn-xs btn-danger"><i class="material-icons">delete</i> Hapus</button>
                ';
            })
            ->make(true);
    }

    public function create()
    {
        $data['title'] = "Tambah Data Siswa";


        $data['breadcrumb'] = array(
            url('/admin')        => "Beranda",
            url('/admin/admin')  => "Data Siswa",
            ''              => "Tambah Data Siswa"
        );
        return view('admin.student.add_student',$data);
    }

    public function edit($nis = null)
    {
        $data['title'] = "Edit Data Siswa";

        $data['student'] = Student::where("nis",$nis)->first();

        $data['breadcrumb'] = array(
            url('/admin')        => "Beranda",
            url('/admin/admin')  => "Data Siswa",
            ''              => "Edit Data Siswa"
        );
        return view('admin.student.edit_student',$data);
    }

    public function update(StudentRequest $request){

        $student = Student::where("nis",$request->nis);
        $object = array(
            'name' => $request->name,
            'birth_date' => $request->birth_date,
            'birth_place' => $request->birth_place,
            'address' => $request->address,
            'email' => $request->email,
            'phone' => $request->phone,
            'city' => $request->city,
            'father' => $request->father,
            'mother' => $request->mother,
            'status'=>$request->status
        );
        

        $student->update($object);

        return response()->json([
            'success' => true,
            'message' => 'Berhasil mengupdate data siswa'
        ]);
    }

    public function insert(StudentRequest $request){
        
        $student = new Student;
      
        $student->nis = $request->nis;
        $student->name = $request->name;
        $student->birth_date = $request->birth_date;
        $student->birth_place = $request->birth_place;
        $student->address = $request->address;
        $student->email = $request->email;
        $student->phone = $request->phone;
        $student->city = $request->city;
        $student->father = $request->father;
        $student->mother = $request->mother;
        $student->status = $request->status;
     
        

        $student->save();

        return response()->json([
            'success' => true,
            'message' => 'Berhasil menambah data siswa'
        ]);
    }

    public function delete(StudentRequest $request){
        $student = Student::where("nis",$request->nis);
        $student->delete();
        return "success";
    }
}
