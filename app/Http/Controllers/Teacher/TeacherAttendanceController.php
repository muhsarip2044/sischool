<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AttendanceStoreRequest;

use Auth;
use Datatables;


use App\Teacher;
use App\Lesson;
use App\Lesson_group;
use App\Kelas;
use App\Student;
use App\Student_attendance;

class TeacherAttendanceController extends Controller
{
    public function index($lesson_group_id=null,Request $request)
    {

        $date  = date("Y-m-d",time());

        $data['title'] = "Data Siswa";
        $data['breadcrumb'] = array(
            url('/teacher')    => "Beranda",
            url('/teacher/lesson')    => "Mata Pelajaran",
            ""          => "Absensi Siswa"
        );

        // filter date
        if ($request->date != null) {
            $date  = $request->date;
        }

        $data['attendance'] = Lesson_group::find($lesson_group_id)->with(array("kelas_instance"=>function($query){
            $query->with("student")->get();
        }))->with("lesson")->first();

        $data['record_attendance'] = Student_attendance::where("lesson_group_id",$lesson_group_id)->where("date",$date)->get();

        return view('teacher.attendance.list_attendance',$data);
    }
    public function store(AttendanceStoreRequest $request){
        // delete attendance
        $check_attendance = Student_attendance::where("lesson_group_id",$request->lesson_group_id)->where("date",$request->date)->get();
        if (count($check_attendance)>0) {
            // delete the data
            $attendance_to_delete = Student_attendance::where("lesson_group_id",$request->lesson_group_id)->where("date",$request->date);
            
            $attendance_to_delete->delete();
        }
        
        $nis_collection = explode(",",$request->nis_collection);


        foreach ($nis_collection as $key ) {
            // get ststus attendance
            $var_nis = "attendance_".$key;
            $var_nis = $request->$var_nis;

            if ($var_nis != null) {
    
                $object_attendance = new Student_attendance;
                $object_attendance->lesson_group_id = $request->lesson_group_id;
                $object_attendance->nis = $key;
                $object_attendance->$var_nis  = 1;
                $object_attendance->date = $request->date;
    
                $object_attendance->save();
            }
            
        }

        return response()->json([
            'success' => true,
            'message' => 'Berhasil mencatat absensi siswa '
        ]);
    }
}
