<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherDashboardController extends Controller
{
    public function dashboard()
    {
        $data['title'] = "Dashboard";

        $data['modul'] = array(
            array(
                "icon"  => "people",
                "title" => "Mata Pelajaran",
                "url"   => url('teacher/lesson'),
                "description"=> "Mata Pelajaran yang di tugaskan"
            ),
        );

        return view('teacher.teacher_dashboard',$data);
    }
}
