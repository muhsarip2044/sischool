<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Datatables;

use App\Teacher;
use App\Lesson;
use App\Lesson_group;
use App\Kelas;

class TeacherLessonController extends Controller
{
    public function index()
    {
        $data['title'] = "Data Siswa";
        $data['breadcrumb'] = array(
            url('/teacher')    => "Beranda",
            ""          => "Mata Pelaaran"
        );
        return view('teacher.lesson.list_lesson',$data);
    }
    public function getDtRowData()
    {
        $lessons = Lesson_group::where("teacher_id",Auth::guard("teacher")->user()->id)->with("kelas")->with("lesson")->get();

        return Datatables::of($lessons)
            ->addColumn('action', function ($lesson) {
                return '
                <a href="'.url('teacher/lesson/attendance/'.$lesson->id).'" class="btn btn-xs btn-primary">
                    <i class="material-icons">mode_edit</i> Absensi
                </a>
                <a href="'.url('teacher/lesson/score/'.$lesson->id).'" class="btn btn-xs btn-primary">
                    <i class="material-icons">mode_edit</i> Penilaian
                </a>
                ';
            })
            ->addIndexColumn()
            ->make(true);
    } 
}
