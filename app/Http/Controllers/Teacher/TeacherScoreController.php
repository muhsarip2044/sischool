<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Http\Requests\StudentScoreRequest;

use Auth;
use Datatables;

use App\Lesson;
use App\Lesson_group;
use App\Score;
use App\Student_score;


class TeacherScoreController extends Controller
{
    public function index($lesson_group_id=null,Request $request)
    {
        $data['common'] = Lesson_group::find($lesson_group_id)->with(
            array(
                "kelas_instance"=>function($query){
                     $query->with( array("student"=>function($querys){
                        $querys->get(["nis","name"]);
                        $querys->with('student_score');
                    }))->get();
                },
                "lesson"
            ))->first();
            //echo dd( $data['common']);
        $data['title'] = "Nilai Mata Pelajaran".$data['common']->lesson->name;
        $data['breadcrumb'] = array(
            url('/teacher')    => "Beranda",
            url('/teacher/lesson')    => "Mata Pelajaran ",
            ''  => "Nilai Mata Pelajaran ".$data['common']->lesson->name
        );

        $data['record_score'] = Student_score::where("lesson_group_id",$lesson_group_id)->get();

        return view('teacher.score.list_score',$data);
    }
    public function store(StudentScoreRequest $request){
        
        // convert NIS collection to array
        $nis_collection = explode(",",$request->nis_collection);

        // itterate all NIS code
        if (count($nis_collection)>0) {
            foreach ($nis_collection as $nis ) {
                
                // get all type of score
                $score_type = Score::all();
                if (count($score_type)>0) {
                    foreach ($score_type as $score) {

                        // check is score exist
                        if (isset($request->score[$nis]['type_'.$score->id.'']) and $request->score[$nis]['type_'.$score->id.''] != '') {
                            
                            // insert / update
                            $student_score = Student_score::updateOrCreate(
                                [
                                    'score_id' => $score->id,
                                    'lesson_group_id'=> $request->lesson_group_id,
                                    'nis'       => $nis,
                                ],
                                [
                                    'score_id' => $score->id,
                                    'lesson_group_id'=> $request->lesson_group_id,
                                    'nis'       => $nis,
                                    'value'     => $request->score[$nis]['type_'.$score->id.'']
                                ]
                            );
                            $student_score->save();


                        }

                        
                    }
                }


            }
        }

        

        return response()->json([
            'success' => true,
            'message' => 'Berhasil memperbaharui nilai'
        ]);
    }
}
