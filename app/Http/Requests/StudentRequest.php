<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

use Auth;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nis'  => 'required',
            'class_id'  => 'integer',
            'name'  => 'required|max:255',
            'birth_date'  => 'date_format:Y-m-d|nullable',
            'birth_place'  => 'max:100',
            'address'  => 'max:500',
            'email'  => 'max:20|email|nullable',
            'phone'  => 'digits:15|nullable',
            'city'  => 'max:100',
            'father'  => 'max:150',
            'mother'  => 'max:150'
        ];

        if (request()->isMethod('post')) {
            $rules['nis'] = 'required';
        }
        if (request()->isMethod('delete')) {
            $rules = [
                'nis'  => 'required',
            ];
        }

        return $rules;
    }
}
