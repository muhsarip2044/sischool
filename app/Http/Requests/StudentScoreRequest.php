<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Auth;

class StudentScoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard("teacher")->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'score.*.*'  => 'numeric|max:101|nullable',
            
        ];

        // if (request()->isMethod('post')) {
        //     $rules['nis'] = 'required|max:50';
        // }
        // if (request()->isMethod('delete')) {
        //     $rules = [
        //         'nis'  => 'required|max:50',
        //     ];
        // }

        return $rules;
    }
}
