<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = "class";
    public function lesson_group()
    {
        return $this->hasMany('App\Lesson_group','class_id','id');
    }

    public function student()
    {
        return $this->hasMany('App\Student','class_id','id');
    }
}
