<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    public function lesson_group()
    {
        return $this->hasMany('App\Lesson_group');
    }
}
