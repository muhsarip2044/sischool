<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson_group extends Model
{
    public function teacher()
    {
        return $this->belongsTo('App\Teacher');
    }
    public function lesson()
    {
        return $this->belongsTo('App\Lesson');
    }
    public function kelas()
    {
        return $this->belongsTo('App\Kelas','id');
    }
    public function kelas_instance()
    {
        return $this->hasOne('App\Kelas','id','class_id');
    }



}
