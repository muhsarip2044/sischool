<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student_score extends Model
{
    protected $fillable = ['score_id', 'nis', 'lesson_group_id','value'];

    public function score()
    {
        return $this->belongsTo('App\Score','id');
    }
}
