@extends('teacher.layouts.master')

@section('scripttop') 
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<!-- Sweetalert Css -->
<link href="{{ asset('plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<style>
.demo-checkbox label, .demo-radio-button label {
    min-width:20px !important;
}
</style>
@stop



@section('content')
<div class="container-fluid">
    @if(count($breadcrumb)>0)
    <div class="block-header">
        <ol class="breadcrumb">
            @foreach($breadcrumb as $key => $value)
            <li><a href="{{ $key }}">{{ $value }}</a></li>
            @endforeach
        </ol> 
    </div>
    @endif
    <!-- Body Copy -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
            <form id="frm-attendance-store" class="form-horizontal form-post" action="{{ url('teacher/lesson/attendance/store') }}" method="post" empty-on-success="false" >
                <input type="hidden" name="lesson_group_id" value="{{ $attendance->id }}">
                <div class="header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h2>
                                Absensi Mata Pelajaran {{ $attendance->lesson->name }} <br>
                                Kelas : {{ $attendance->kelas_instance->class }}
                            </h2>
                        </div>
                        <div class="col-lg-6">
                            <div class="row ">
                                <div class="col-lg-2">
                                    <h2 style="padding-top:10px">
                                        Tanggal
                                    </h2>
                                </div>
                                <div class="col-lg-5">
                                    @php 
                                    $date_selected = isset($_GET['date'])?$_GET['date']:'';
                                    @endphp
                                    <input type="text" id="date_filter" name="date" class="form-control datepicker col-lg-8" value="{{ $date_selected==''?date('Y-m-d',time()):$date_selected }}">
                                </div>
                                <div class="col-lg-5">
                                    <button  type="submit" class="btn btn-primary btn-cyan">Simpan Absen{{ request()->route('date') }}</button>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    @php 
                    $record_collection = array();
                    @endphp
                    @if(count($record_attendance)>0)
                        @foreach($record_attendance as $key)
                            @php 
                            $record_collection[$key->nis] = $key; 
                            @endphp
                        @endforeach
                    @endif
                    
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIS</th>
                                <th>Nama Siswa</th>
                                <th>
                                    <div class="demo-checkbox">
                                        <input type="checkbox"  id="set_all_present" class="selector-attendance">
                                        <label for="set_all_present"><strong>Hadir</strong></label>
                                    </div>
                                </th>
                                <th>
                                    <div class="demo-checkbox">
                                        <input type="checkbox"  id="set_all_permision" class="selector-attendance">
                                        <label for="set_all_permision"><strong>Izin</strong></label>
                                    </div>
                                </th>
                                <th>
                                    <div class="demo-checkbox">
                                        <input type="checkbox"  id="set_all_sick" class="selector-attendance">
                                        <label for="set_all_sick"><strong>Sakit</strong></label>
                                    </div>
                                </th>
                                <th>
                                    <div class="demo-checkbox">
                                        <input type="checkbox"  id="set_all_alpha" class="selector-attendance">
                                        <label for="set_all_alpha"><strong>Alpha</strong></label>
                                    </div>
                                </th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($attendance->kelas_instance->student)>0)
                                @foreach($attendance->kelas_instance->student as $key)
                                    <tr>
                                        <td>{{ $loop->index+1 }}</td>
                                        <td>{{ $key->nis }}</td>
                                        <td>{{ $key->name }}</td>
                                        <td style="width:5px;text-align:center">
                                            <div class="demo-radio-button" style="width:5px;margin-left:20px">
                                                <input 
                                                    name="attendance_{{ $key->nis }}" 
                                                    type="radio" 
                                                    id="present{{ $key->nis }}" 
                                                    value="present" 
                                                    class="radio-col-green" 
                                                    {!! isset($record_collection[$key->nis])?($record_collection[$key->nis]['present']==1?'checked':''):'' !!}
                                                    >
                                                <label for="present{{ $key->nis }}" ></label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="demo-radio-button" style="width:5px">
                                                <input 
                                                    name="attendance_{{ $key->nis }}" 
                                                    type="radio" 
                                                    id="permision{{ $key->nis }}" 
                                                    value="permision" 
                                                    class="radio-col-blue" 
                                                    {!! isset($record_collection[$key->nis])?($record_collection[$key->nis]['permision']==1?'checked':''):'' !!}
                                                >
                                                <label for="permision{{ $key->nis }}" ></label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="demo-radio-button" style="width:5px">
                                                <input 
                                                    name="attendance_{{ $key->nis }}" 
                                                    type="radio" 
                                                    id="sick{{ $key->nis }}" 
                                                    value="sick" 
                                                    class="radio-col-yellow" 
                                                    {!! isset($record_collection[$key->nis])?($record_collection[$key->nis]['sick']==1?'checked':''):'' !!}
                                                >
                                                <label for="sick{{ $key->nis }}" ></label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="demo-radio-button" style="width:5px">
                                                <input 
                                                    name="attendance_{{ $key->nis }}" 
                                                    type="radio" 
                                                    id="alpha{{ $key->nis }}" 
                                                    value="alpha" 
                                                    class="radio-col-red" 
                                                    {!! isset($record_collection[$key->nis])?($record_collection[$key->nis]['alpha']==1?'checked':''):'' !!}
                                                >
                                                <label for="alpha{{ $key->nis }}" ></label>
                                            </div>
                                        </td>
                                        <td>
                                            <textarea name="note_{{ $key->nis }}" id="" cols="20" rows="1"></textarea>
                                        </td>
                                    </tr>
                                @endforeach
                                @php 
                                $nis_collection = implode(",",array_pluck($attendance->kelas_instance->student, 'nis'));
                                
                                @endphp
                                <input type="hidden" name="nis_collection" value="{{ $nis_collection }}">
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3">Total</th>
                                <th><span id="total_present"></span> Hadir</th>
                                <th><span id="total_permision"></span> Izin</th>
                                <th><span id="total_sick"></span> Sakit</th>
                                <th><span id="total_alpha"></span> Alpha</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('scriptbottom')
<!-- Moment Plugin Js -->
<script src="{{ asset('plugins/momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

<!-- SweetAlert Plugin Js -->
<script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>

<script>
// present
$("#set_all_present").on("change",function(){
    $("input:checkbox.selector-attendance").prop('checked', false);
    $(this).prop('checked', true);


    if ($(this).is(':checked')) {
        $('input:radio[class=radio-col-green]').prop('checked', true);
    }else{
        $('input:radio[class=radio-col-green]').prop('checked', false);
    }
    
    
});

// permision
$("#set_all_permision").on("change",function(){
    $("input:checkbox.selector-attendance").prop('checked', false);
    $(this).prop('checked', true);

    if ($(this).is(':checked')) {
        $('input:radio[class=radio-col-blue]').prop('checked', true);
    }else{
        $('input:radio[class=radio-col-blue]').prop('checked', false);
    }
    
});

// sick
$("#set_all_sick").on("change",function(){
    $("input:checkbox.selector-attendance").prop('checked', false);
    $(this).prop('checked', true);

    if ($(this).is(':checked')) {
        $('input:radio[class=radio-col-yellow]').prop('checked', true);
    }else{
        $('input:radio[class=radio-col-yellow]').prop('checked', false);
    }
    
});

// alpha
$("#set_all_alpha").on("change",function(){
    $("input:checkbox.selector-attendance").prop('checked', false);
    $(this).prop('checked', true);

    if ($(this).is(':checked')) {
        $('input:radio[class=radio-col-red]').prop('checked', true);
    }else{
        $('input:radio[class=radio-col-red]').prop('checked', false);
    }
    
});

// count each type
$(".demo-radio-button > input, .demo-checkbox > input").on("change",function(){

    $("#total_present").text($('input.radio-col-green:checked').length);
    $("#total_permision").text($('input.radio-col-blue:checked').length);
    $("#total_sick").text($('input.radio-col-yellow:checked').length);
    $("#total_alpha").text($('input.radio-col-red:checked').length);
});

// date format
$('.datepicker').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD',
    clearButton: true,
    weekStart: 1,
    time: false
});

// filter date
$("#date_filter").on("change",function(){
    window.location.replace("{{ url('teacher/lesson/attendance/'.$attendance->id) }}?date="+ $(this).val());
})
</script>
@stop