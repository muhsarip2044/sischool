<!-- Jquery Core Js -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ asset('js/bootstrap.js') }}"></script>

<!-- Select Plugin Js -->
<!-- <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script> -->

<!-- Bootstrap Notify Plugin Js -->
<script src="{{ asset('plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

<!-- Custom Js -->
<script src="{{ asset('js/admin.js') }}"></script>

<!-- Demo Js -->
<script src="{{ asset('js/demo.js') }}"></script>

<script>
$(".form-post").on("submit",function(e){
    e.preventDefault();
    var data = $(this).serialize();

    // remove all error
    $(".label-field").html("").fadeOut();
    $(".input-field").removeClass("error");

    // handle button
    var button_html = $(this).find("button[type=submit]").text();
    $(this).find("button[type=submit]").attr("disabled",true);
    $(this).find("button[type=submit]").html("Memproses ....");

    // handle url
    var url = $(this).attr("action");

    var instance = this;

    // handle on save
    var empty_on_success = $(this).attr("empty-on-success");

    setTimeout(function(){ 
        $.post(url,data, function(response){
            if (response.success == true) {
                $.notify({
                    message: response.message 
                },{
                    type: 'success',
                    placement: {
                        from: "bottom",
                        align: "right"
                    },
                });

                // check empty on success
                if (empty_on_success == "true") {
                    $(".input-field > input").val("");
                }
            }else{
                $.notify({
                    message: response.message 
                },{
                    type: 'error',
                    placement: {
                        from: "bottom",
                        align: "right"
                    },
                });
            }
            // enable button submit
            $(instance).find("button[type=submit]").html(button_html).attr("disabled",false);
            $(instance).find("button[type=submit]").attr("disabled",false);

           
        },"json").fail(function(response) {
            $(instance).find("button[type=submit]").html(button_html).attr("disabled",false);
            $(instance).find("button[type=submit]").attr("disabled",false);
        });
    }, 1000);

    
})
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    error: function(xhr){
        
        // handle error validation with laravel validation response
        if (xhr.status == 422) {
            $(".label-field").html("").fadeOut();
            $(".input-field").removeClass("error");


            var err_json = JSON.parse(xhr.responseText);
            var i = 1;
            for (var err in err_json){
                if (typeof err_json[err] !== 'function') {
                    //alert("Key is " + err + ", value is" + err_json[err]);
                    // set class error in field
                    $("#validation-" +err+ " > .input-field").addClass("error");

                    // set label error
                    $("#validation-" +err+ " > .label-field").html(err_json[err]).fadeIn();

                    // set focus
                    if (i == 1) {
                        $("#validation-" +err+ " > .input-field > input").focus();
                    }
                }
                i++;
            }

            // addition single/global message
            if( $('#global-message').length )         
            {
                $('#global-message').html("Invalid input format").fadeIn();
            }
        }
    }
});
</script>