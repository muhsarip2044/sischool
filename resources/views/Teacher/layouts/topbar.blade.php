<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="{{ url('/admin') }}" style="padding:0px">
                <img src="{{ asset('images/scdocs.png') }}" style="height:53px;width:auto;margin-left:80px" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                <!-- #END# Call Search -->
                <!-- Tasks -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">person_outline</i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header"><span class="col-cyan">Hi</span>, {{ Auth::guard('teacher')->user()->name }}</li>
                        <li class="body">
                            <ul class="menu tasks">
                                <li>
                                    <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        <i class="material-icons" style="font-size:15px;width:30px;">person_outline</i> 
                                        Profil Saya
                                    </a>
                                </li>
                                <li>
                                    <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        <i class="material-icons" style="font-size:15px;width:30px;">build</i> 
                                        Ubah Password
                                    </a>
                                </li>
                                <li>
                                    <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        <i class="material-icons" style="font-size:15px;width:30px;">input</i> 
                                        Keluar
                                    </a>
                                </li>
                               
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- #END# Tasks -->
                <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">settings</i></a></li>
            </ul>
            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</nav>
<!-- #Top Bar -->