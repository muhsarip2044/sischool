@extends('teacher.layouts.master')

@section('scripttop') 
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<!-- Sweetalert Css -->
<link href="{{ asset('plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />

<!-- Bootstrap Select Css -->
<link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

<style>
.demo-checkbox label, .demo-radio-button label {
    min-width:20px !important;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}
</style>
@stop



@section('content')
<div class="container-fluid">
    @if(count($breadcrumb)>0)
    <div class="block-header">
        <ol class="breadcrumb">
            @foreach($breadcrumb as $key => $value)
            <li><a href="{{ $key }}">{{ $value }}</a></li>
            @endforeach
        </ol> 
    </div>
    @endif
    <!-- Body Copy -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
            <form id="frm-attendance-store" class="form-horizontal form-post" action="{{ url('teacher/lesson/score/store') }}" method="post" empty-on-success="false" >
                <input type="hidden" name="lesson_group_id" value="{{ $common->id }}">
                <div class="header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h2>
                                Nilai Mata Pelajaran {{ $common->lesson->name }} <br>
                                Kelas : {{ $common->kelas_instance->class }}
                            </h2>
                        </div>
                        <div class="col-lg-6">
                            <div class="row clearfix">
                                <div class="col-lg-5">
                                    <select class="form-control show-tick">
                                        <option value="1">Semester 1</option>
                                        <option value="2">Semester 2</option>
                                    </select>
                                </div>
                                <div class="col-lg-7 text-right">
                                    <button  type="submit" class="btn btn-primary btn-cyan m-r-20">Perbarui Nilai</button>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    @php 
                    $record_collection = array();
                    @endphp
                    @if(count($record_score)>0)
                        @foreach($record_score as $key)
                            @php 
                            $record_collection[$key->nis] = $key; 
                            @endphp
                        @endforeach
                    @endif
                    
                    <div id="global-message" class="label-field alert alert-danger" style="display:none"></div>
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIS</th>
                                <th>Nama Siswa</th>
                                <th style="text-align:center">Nilai Tugas</th>
                                <th style="text-align:center">Nilai UTS</th>
                                <th style="text-align:center">Nilai UAS</th>
                                <th style="text-align:center">Nilai Absensi</th>
                                <th style="text-align:center">Nilai Akhir</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($common->kelas_instance->student)>0)
                                @foreach($common->kelas_instance->student as $key)
                                    <tr>
                                        <td>{{ $loop->index+1 }}</td>
                                        <td>{{ $key->nis }}</td>
                                        <td>{{ $key->name }}</td>
                                        <td align="center">
                                            @php 
                                            $collection = collect($key->student_score);
                                            $collection = $collection->filter(function($consumer){
                                                return $consumer->score_id == 1;
                                            })->first();
                                            @endphp
                                            <input 
                                                name="score[{{ $key->nis }}][type_1]" 
                                                value="{{ (isset($collection->value)?$collection->value:'') }}" 
                                                type="number" 
                                                size="3" 
                                                style="text-align:center;width:40px"
                                            >
                                        </td>
                                        <td align="center">
                                            @php 
                                            $collection = collect($key->student_score);
                                            $collection = $collection->filter(function($consumer){
                                                return $consumer->score_id == 2;
                                            })->first();
                                            @endphp
                                            <input 
                                                name="score[{{ $key->nis }}][type_2]" 
                                                value="{{ (isset($collection->value)?$collection->value:'') }}" 
                                                type="number" 
                                                size="3" 
                                                style="text-align:center;width:40px"
                                            >
                                        </td>
                                        <td align="center">
                                            @php 
                                            $collection = collect($key->student_score);
                                            $collection = $collection->filter(function($consumer){
                                                return $consumer->score_id == 3;
                                            })->first();
                                            @endphp
                                            <input 
                                                name="score[{{ $key->nis }}][type_3]" 
                                                value="{{ (isset($collection->value)?$collection->value:'') }}" 
                                                type="number" 
                                                size="3" 
                                                style="text-align:center;width:40px"
                                            >
                                        </td>
                                        <td align="center">
                                            @php 
                                            $collection = collect($key->student_score);
                                            $collection = $collection->filter(function($consumer){
                                                return $consumer->score_id == 5;
                                            })->first();
                                            @endphp
                                            <input 
                                                name="score[{{ $key->nis }}][type_5]" 
                                                value="{{ (isset($collection->value)?$collection->value:'') }}" 
                                                type="number" 
                                                size="3" 
                                                style="text-align:center;width:40px"
                                            >
                                        </td>
                                        <td align="center">
                                            @php 
                                            $collection = collect($key->student_score);
                                            $collection = $collection->filter(function($consumer){
                                                return $consumer->score_id == 4;
                                            })->first();
                                            @endphp
                                            <input 
                                                name="score[{{ $key->nis }}][type_4]" 
                                                value="{{ (isset($collection->value)?$collection->value:'') }}" 
                                                type="number" 
                                                size="3" 
                                                style="text-align:center;width:40px"
                                            >
                                        </td>
                                    </tr>
                                @endforeach
                                @php 
                                $nis_collection = implode(",",array_pluck($common->kelas_instance->student, 'nis'));
                                
                                @endphp
                                <input type="hidden" name="nis_collection" value="{{ $nis_collection }}">
                            @endif
                        </tbody>
                        <tfoot style="display:none">
                            <tr>
                                <th colspan="3">Nilai Rata-rata</th>
                                <th><span id="total_present"></span></th>
                                <th><span id="total_permision"></span> </th>
                                <th><span id="total_sick"></span> </th>
                                <th><span id="total_alpha"></span> </th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('scriptbottom')
<!-- Moment Plugin Js -->
<script src="{{ asset('plugins/momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

<!-- SweetAlert Plugin Js -->
<script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Select Plugin Js -->
<script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

<script>
// present
$("#set_all_present").on("change",function(){
    $("input:checkbox.selector-attendance").prop('checked', false);
    $(this).prop('checked', true);


    if ($(this).is(':checked')) {
        $('input:radio[class=radio-col-green]').prop('checked', true);
    }else{
        $('input:radio[class=radio-col-green]').prop('checked', false);
    }
    
    
});

// permision
$("#set_all_permision").on("change",function(){
    $("input:checkbox.selector-attendance").prop('checked', false);
    $(this).prop('checked', true);

    if ($(this).is(':checked')) {
        $('input:radio[class=radio-col-blue]').prop('checked', true);
    }else{
        $('input:radio[class=radio-col-blue]').prop('checked', false);
    }
    
});

// sick
$("#set_all_sick").on("change",function(){
    $("input:checkbox.selector-attendance").prop('checked', false);
    $(this).prop('checked', true);

    if ($(this).is(':checked')) {
        $('input:radio[class=radio-col-yellow]').prop('checked', true);
    }else{
        $('input:radio[class=radio-col-yellow]').prop('checked', false);
    }
    
});

// alpha
$("#set_all_alpha").on("change",function(){
    $("input:checkbox.selector-attendance").prop('checked', false);
    $(this).prop('checked', true);

    if ($(this).is(':checked')) {
        $('input:radio[class=radio-col-red]').prop('checked', true);
    }else{
        $('input:radio[class=radio-col-red]').prop('checked', false);
    }
    
});

// count each type
$(".demo-radio-button > input, .demo-checkbox > input").on("change",function(){

    $("#total_present").text($('input.radio-col-green:checked').length);
    $("#total_permision").text($('input.radio-col-blue:checked').length);
    $("#total_sick").text($('input.radio-col-yellow:checked').length);
    $("#total_alpha").text($('input.radio-col-red:checked').length);
});

// date format
$('.datepicker').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD',
    clearButton: true,
    weekStart: 1,
    time: false
});

// filter date
$("#date_filter").on("change",function(){
    window.location.replace("{{ url('teacher/lesson/attendance/'.$common->id) }}?date="+ $(this).val());
})
</script>
@stop