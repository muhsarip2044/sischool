@extends('admin.layouts.master')

@section('scripttop') 

@stop



@section('content')
<div class="container-fluid">
    <div class="block-header text-center">
        <img src="{{ asset('images/scdocs.png') }}" style="height:100px;width:auto;" alt="">
        <h5 class="color-main">Selamat datang {{ strtoupper($user->name) }}</h5>
    </div>
    <div class="row clearfix dwidget">

        @if(count($modul))
            @for($i=0;$i<count($modul);$i++)
                <div class="col-lg-4 dwidget-item">
                    <div class="card">
                        <div class="header">
                            <a href="{{ $modul[$i]['url'] }}">
                                <div class="row">
                                    <div class="col-lg-1">
                                        <i class="material-icons text-center" style="font-size:44px;padding-top:15px">{{ $modul[$i]['icon'] }}</i>
                                    </div>
                                    <div class="col-lg-6">
                                        <h3 class="text-center main-color" style="font-size:24px;padding-top:5px">
                                        {{ $modul[$i]['title'] }}
                                        </h3>
                                    </div>
                                    <div class="col-lg-4 text-right">
                                        <h3 ><i class="material-icons" style="font-size:40px">keyboard_arrow_right</i></h3>
                                    </div>
                                </div>
                            </a>
                            
                        </div>
                        <div class="body">
                            {{ $modul[$i]['description'] }}
                        </div>
                    </div>
                </div>
            @endfor
        @endif
    </div>
</div>
@stop

@section('scriptbottom')

@stop