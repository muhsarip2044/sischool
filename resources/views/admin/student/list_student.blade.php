@extends('admin.layouts.master')

@section('scripttop') 
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<!-- Sweetalert Css -->
<link href="{{ asset('plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@stop



@section('content')
<div class="container-fluid">
    @if(count($breadcrumb)>0)
    <div class="block-header">
        <ol class="breadcrumb">
            @foreach($breadcrumb as $key => $value)
            <li><a href="{{ $key }}">{{ $value }}</a></li>
            @endforeach
        </ol> 
    </div>
    @endif
    <!-- Body Copy -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Table Siswa
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">

                    <button class="btn bg-cyan waves-effect m-r-20 ">
                        <i class="material-icons">file_upload</i> Import Data Siswa
                    </button>

                    <a href="{{ url('admin/student/create') }}" class="btn bg-cyan waves-effect ">
                        <i class="material-icons">person_add</i> Tambah Data Siswa
                    </a>
                    
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>NIS</th>
                                <th>Nama Siswa</th>
                                <th>Kota</th>
                                <th style="width: 100px;"></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>NIS</th>
                                <th>Nama Siswa</th>
                                <th>Kota</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scriptbottom')
<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

<!-- SweetAlert Plugin Js -->
<script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>

<script>
    // global var
    var table_student,
    url_list = '{!! url("admin/student/data") !!}',
    url_delete = '{!! url("admin/student/delete") !!}';


    // datatable
    table_student = $('.js-basic-example').DataTable({
        dom: 'Bfrtip',
        buttons: [],
        processing: true,
        serverSide: true,
        ajax: url_list,
        columns: [
            {data: 'nis', name: 'nis'},
            {data: 'name', name: 'name'},
            {data: 'city', name: 'city'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

    // delete function
    
    function delete_data(nis=null) {
        swal({
            title: "Anda yakin ingin menghapus data ini?",
            text: "Anda mungkin tidak dapat mengembalikannya lagi.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, hapus",
            closeOnConfirm: false
        }, function () {
            // send request delete to server
            $.ajax({
                url: url_delete,
                data:{nis:nis},
                type: 'DELETE',
                success: function(response) {
                    table_student.ajax.reload();
                    swal("Terhapus!", "Data telah berhasil terhapus.", "success");
                }
            });            
        });
    }
</script>
@stop