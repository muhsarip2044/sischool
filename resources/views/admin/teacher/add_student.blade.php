@extends('admin.layouts.master')

@section('scripttop') 
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
@stop



@section('content')
<div class="container-fluid">
    <div class="block-header">
        @if(count($breadcrumb)>0)
        <div class="block-header">
            <ol class="breadcrumb">
                @foreach($breadcrumb as $key => $value)
                <li><a href="{{ $key }}">{{ $value }}</a></li>
                @endforeach
            </ol> 
        </div>
        @endif
    </div>
    <!-- Body Copy -->
    <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <form id="frm-add" class="form-horizontal form-post" method="post" action="{{ url('admin/student/insert') }}" empty-on-success="true">
            {{ csrf_field() }}
            <div class="header">
                <div class="text-right">
                    <a href="{{ url('admin/student') }}" class="btn bg-orange waves-effect m-r-20 ">
                        <i class="material-icons">keyboard_backspace</i> KEMBALI
                    </a>
                    <button type="submit" class="btn bg-cyan waves-effect m-r-20 ">
                        SIMPAN
                    </button>
                </div>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Status</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group">
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Belum Lulus</option>
                                    <option value="2">Sudah Lulus</option>
                                    <option value="3">Drop Out</option>
                                </select>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Nomor Induk Siswa</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group "  id="validation-nis">
                            <div class="form-line input-field" input-field>
                                <input type="text" id="nis"  name="nis" class="form-control" placeholder="masukan nis">
                            </div>
                            <label  class="error label-field"  style="display:none"></label>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Nama</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group"  id="validation-name">
                            <div class="form-line input-field">
                                <input type="text" id="name" name="name"  class="form-control" placeholder="masukan nama">
                            </div>
                            <label  class="error label-field"  style="display:none"></label>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Tanggal Lahir</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group id="validation-name"" >
                            <div class="form-line input-field">
                                <input type="text"  id="birth_date" name="birth_date" class="form-control datepicker" >
                            </div>
                            <label  class="error label-birth_date"  style="display:none"></label>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Tempat Lahir</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group"  id="validation-birth_place">
                            <div class="form-line input-field">
                                <input type="text" id="birth_place"  name="birth_place" class="form-control" placeholder="masukan nis">
                            </div>
                            <label  class="error label-field"  style="display:none"></label>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Alamat</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group"  id="validation-address">
                            <div class="form-line input-field">
                                <textarea type="text" id="address" name="address" class="form-control" ></textarea>
                            </div>
                            <label  class="error label-field"  style="display:none"></label>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Email</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group"  id="validation-email">
                            <div class="form-line input-field">
                                <input type="email" id="email"  name="email" class="form-control" placeholder="masukan email">
                            </div>
                            <label  class="error label-field"  style="display:none"></label>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Nomor Telepon</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group "  id="validation-phone">
                            <div class="form-line input-field">
                                <input type="text" id="phone" name="phone"  class="form-control" placeholder="masukan no telepon">
                            </div>
                        </div>
                        <label  class="error label-field"  style="display:none"></label>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Kota</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group"  id="validation-city">
                            <div class="form-line input-field">
                                <input type="text" id="city" name="city"  class="form-control" placeholder="masukan kota">
                            </div>
                        </div>
                        <label  class="error label-field"  style="display:none"></label>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Nama Bapak</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group"  id="validation-father">
                            <div class="form-line input-field">
                                <input type="text" id="father" name="father"  class="form-control" placeholder="masukan nama bapak">
                            </div>
                            <label  class="error label-field"  style="display:none"></label>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Nama Ibu</label>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group"  id="validation-mother">
                            <div class="form-line input-field">
                                <input type="text" id="mother" name="mother"  class="form-control" placeholder="masukan nama ibu">
                            </div>
                            <label  class="error label-field"  style="display:none"></label>
                        </div>
                    </div>
                </div>

            </div>
            </form>
        </div>
    </div>
</div>
</div>
@stop

@section('scriptbottom')
<!-- Moment Plugin Js -->
<script src="{{ asset('plugins/momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script>
$('.datepicker').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD',
    clearButton: true,
    weekStart: 1,
    time: false
});
</script>
@stop