
<style>
.website-nav {
    margin:70px 0px 20px 25px !important;
    
}
.website-nav li {
    display:inline;
    list-style:none;
    margin-right:40px;
}
.website-nav li a {
    text-decoration:none;
    font-family: 'Roboto', Arial, Tahoma, sans-serif;
    color:#848484;
}
.website-nav li a:hover {
    color:#448aff;
}
</style>
<!-- Top Bar -->


<nav class="navbar">
    <div class="container-fluid website-navbar">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="{{ url('/') }}" style="padding:0px">
                <img src="{{ asset('images/scdocs.png') }}" style="height:53px;width:auto;margin-left:80px" alt="">
            </a>
            <ul class="website-nav">
                <li><a href="">Beranda</a></li>
                <li><a href="">Berita</a></li>
                <li><a href="">Pengumuman</a></li>
            </ul>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                <!-- #END# Call Search -->
                <!-- Tasks -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="color:#333 !important">
                        <div style="float:left;margin-right:10px">LOGIN</div> <i class="material-icons" style="color:#333;font-size:18px">input</i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">LOGIN SEBAGAI</li>
                        <li class="body">
                            <ul class="menu tasks text-center">
                                <li>
                                    <a href="{{ url('teacher/login') }}">
                                        <h4>
                                            GURU
                                        </h4>
                                        <div>
                                            <img src="{{ asset('images/teacher.png') }}" style="height:70px" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('student/login') }}">
                                        <h4>
                                            SISWA
                                        </h4>
                                        <div>
                                            <img src="{{ asset('images/student.png') }}" style="height:70px" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('login') }}">
                                        <h4>
                                            ADMIN
                                        </h4>
                                        <div>
                                            <img src="{{ asset('images/admin.png') }}" style="height:70px" alt="">
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- #END# Tasks -->
                <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">settings</i></a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->