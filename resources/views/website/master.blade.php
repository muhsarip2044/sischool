<!DOCTYPE html>
<html>

<head>
    @include('website.layouts.sourcetop')
    @yield('scripttop')
</head>

<body class="theme-cyan">
<style>
body {
    background:#f6f6f6 !important;
}
.website-navbar {
    background:#fff;
}
section.content {
    margin: 100px 0px 0 0px !important;
}
</style>
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->

    @include('website.layouts.topbar')

    

    <section class="content">
        @yield('content')
    </section>

    @include('website.layouts.sourcebottom')
    @yield('scriptbottom')
</body>

</html>