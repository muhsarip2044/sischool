@extends('website.master')

@section('scripttop') 
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
@stop



@section('content')
<style>
.banner-title {
    color:#444;
    font-family: 'Roboto Slab',palatino,serif;
    font-size: 58px;
    font-weight: 300;
    letter-spacing: -.2px;
    line-height: 38px;
    margin-bottom:20px;
}
.banner-sub-title {
    font-weight:lighter;
    font-size:20px;
    margin-top:40px;
    font-family: 'Roboto',arial,sans-serif;
}
.testimoni {
    background:#00b0ff;
    padding:100px;
    text-align:center;
    
}
.testimoni .title {
    font-size:40px;
    color:#fff;
    font-family: 'Roboto Slab',palatino,serif;
}
.testimoni .sub-title {
    margin-top:30px;
    font-size:15px;
    color:#fff;
    font-family: 'Roboto Slab',palatino,serif;
}
</style>
<div class="container-fluid">
    <div class="row clearfix dwidget">
        <div class="col-lg-6" style="padding:0px !important">
            <img src="{{ asset('images/main-banner.jpg') }}" style="width:100%" alt="">
        </div>
        <div class="col-lg-6">
            <div style="padding:100px">
                <h1 class="banner-title">
                    SCDOCS
                </h1>
                <p class="banner-sub-title">
                    Sekolah idaman untuk murid berprestasi. Mantapkan semangatmu belajar dan terus berlajar.
                </p>
            </div>
        </div>
        <div class="col-lg-12 testimoni" >
            <div class="title">
                "Sekolah yang tidak akan membiarkan bakat-bakat muridnya terpendam begitu lama"
            </div>
            <div class="sub-title">
                <strong>Raden Wijaya</strong> - Pengamat pendidikan Indoensia
            </div>
        </div>
    </div>
</div>
@stop

@section('scriptbottom')

@stop