<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin','namespace' => 'Admin','middleware' => 'auth'], function () {
    Route::get('/', 'AdminDashboardController@dashboard')->name('admin_dashboard');

    // SISWA
    Route::get('/student', 'AdminStudentController@index')->name('admin_student'); 
    Route::get('/student/data', 'AdminStudentController@getDtRowData')->name('admin_student_data');
    Route::get('/student/create', 'AdminStudentController@create')->name('admin_student_create');
    Route::get('/student/edit/{nis}', ['uses' =>'AdminStudentController@edit'])->name('admin_student_edit');
    Route::post('/student/update', 'AdminStudentController@update')->name('admin_student_update');
    Route::post('/student/insert', 'AdminStudentController@insert')->name('admin_student_insert');
    Route::delete('/student/delete', 'AdminStudentController@delete')->name('admin_student_delete');
});


Route::group(['namespace' => 'Student','middleware' => 'auth'], function () {

    
});







Route::get('/home', 'HomeController@index')->name('website');
Route::get('/', 'HomeController@index')->name('website');




/* =========================================================================================== */
/*                                       TEACHER                                               */
/* =========================================================================================== */

// LOGIN - REGISTER -FORGOT PASSWORD ( BEFORE LOGIN )
Route::group(['prefix' => 'teacher'], function () {
  Route::get('/login', 'TeacherAuth\LoginController@showLoginForm')->name('teacher_login');
  Route::post('/login', 'TeacherAuth\LoginController@login');
  Route::post('/logout', 'TeacherAuth\LoginController@logout')->name('teacher_logout');

  Route::get('/register', 'TeacherAuth\RegisterController@showRegistrationForm')->name('teacher_register');
  Route::post('/register', 'TeacherAuth\RegisterController@register');

  Route::post('/password/email', 'TeacherAuth\ForgotPasswordController@sendResetLinkEmail')->name('teacher_password.request');
  Route::post('/password/reset', 'TeacherAuth\ResetPasswordController@reset')->name('teacher_password.email');
  Route::get('/password/reset', 'TeacherAuth\ForgotPasswordController@showLinkRequestForm')->name('teacher_password.reset');
  Route::get('/password/reset/{token}', 'TeacherAuth\ResetPasswordController@showResetForm');
});

// APP ( AFTER LOGIN )
Route::group(['namespace' => 'Teacher','middleware' => 'auth:teacher','prefix' => 'teacher'], function () {
    // Dashboard
    Route::get('/', 'TeacherDashboardController@dashboard')->name('teacher_dashboard');


    // Lesson
    Route::get('/lesson', 'TeacherLessonController@index')->name('teacher_lesson'); 
    Route::get('/lesson/data', 'TeacherLessonController@getDtRowData')->name('teacher_lesson_data');

    // attendance
    Route::get('/lesson/attendance/{lesson_group_id}', 'TeacherAttendanceController@index')->name("teacher_attendance_list");
    Route::post('/lesson/attendance/store', 'TeacherAttendanceController@store')->name("teacher_attendance_store");

    // score
    Route::get('/lesson/score/{lesson_group_id}', ['uses' =>'TeacherScoreController@index'])->name("teacher_score_list");
    Route::post('/lesson/score/store', 'TeacherScoreController@store')->name("teacher_score_store");
});
/* =========================================================================================== */
/*                                       END OF TEACHER                                        */
/* =========================================================================================== */








Route::group(['prefix' => 'student'], function () {
  Route::get('/login', 'StudentAuth\LoginController@showLoginForm')->name('teacher_login');
  Route::post('/login', 'StudentAuth\LoginController@login');
  Route::post('/logout', 'StudentAuth\LoginController@logout')->name('teacher_logout');

  Route::get('/register', 'StudentAuth\RegisterController@showRegistrationForm')->name('teacher_register');
  Route::post('/register', 'StudentAuth\RegisterController@register');

  Route::post('/password/email', 'StudentAuth\ForgotPasswordController@sendResetLinkEmail')->name('teacher_password.request');
  Route::post('/password/reset', 'StudentAuth\ResetPasswordController@reset')->name('teacher_password.email');
  Route::get('/password/reset', 'StudentAuth\ForgotPasswordController@showLinkRequestForm')->name('teacher_password.reset');
  Route::get('/password/reset/{token}', 'StudentAuth\ResetPasswordController@showResetForm');
});


Auth::routes();

