-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2017 at 06:51 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sischool`
--

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `id` int(11) NOT NULL,
  `class` varchar(3) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `description` text,
  `sort` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `class`, `teacher_id`, `description`, `sort`, `created_at`, `updated_at`) VALUES
(1, '1A', 1, 'Kelas International', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

CREATE TABLE `lessons` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lessons`
--

INSERT INTO `lessons` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Matematika', 'Mata pelajaran', NULL, NULL),
(2, 'Fisika', 'Mata pelajaran', NULL, NULL),
(3, 'Bilogi', 'Mata pelajaran', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lesson_groups`
--

CREATE TABLE `lesson_groups` (
  `id` int(11) NOT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `class_id` int(1) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `creteated_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `grade` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lesson_groups`
--

INSERT INTO `lesson_groups` (`id`, `lesson_id`, `teacher_id`, `class_id`, `status`, `creteated_at`, `updated_at`, `grade`) VALUES
(1, 1, 5, 1, 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_15_150703_create_students_table', 2),
(4, '2017_11_15_155851_create_class_table', 3),
(5, '2017_11_15_155905_create_teachers_table', 4),
(6, '2017_11_18_090158_create_teacher_password_resets_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Nilai Tugas', NULL, NULL, NULL),
(2, 'Nilai UTS', NULL, NULL, NULL),
(3, 'Nilai UAS', NULL, NULL, NULL),
(4, 'Nilai Akhir', NULL, NULL, NULL),
(5, 'Nilai Absensi', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `nis` varchar(50) NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `birth_place` varchar(100) DEFAULT NULL,
  `address` text,
  `email` varchar(20) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `father` varchar(150) DEFAULT NULL,
  `mother` varchar(150) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`nis`, `class_id`, `name`, `birth_date`, `birth_place`, `address`, `email`, `phone`, `city`, `father`, `mother`, `status`, `created_at`, `updated_at`) VALUES
('NS-001', 1, 'Roni', NULL, 'Jakarta', NULL, NULL, NULL, NULL, NULL, NULL, 1, '0000-00-00 00:00:00', '2017-11-26 05:48:03'),
('NS-002', 1, 'Albarsyah', '2009-02-20', 'Jakartav', NULL, NULL, NULL, NULL, NULL, NULL, 1, '0000-00-00 00:00:00', '2017-11-23 14:12:20'),
('NS008', 1, 'Heriawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-11-19 11:27:49', '2017-11-19 11:27:49'),
('NS009', 1, 'Rangga', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-11-19 11:27:33', '2017-11-19 11:27:33');

-- --------------------------------------------------------

--
-- Table structure for table `student_attendances`
--

CREATE TABLE `student_attendances` (
  `id` int(11) NOT NULL,
  `lesson_group_id` int(11) DEFAULT NULL,
  `nis` varchar(50) DEFAULT NULL,
  `present` int(1) DEFAULT NULL,
  `alpha` int(1) DEFAULT NULL,
  `sick` int(1) DEFAULT NULL,
  `permision` int(1) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_attendances`
--

INSERT INTO `student_attendances` (`id`, `lesson_group_id`, `nis`, `present`, `alpha`, `sick`, `permision`, `note`, `created_at`, `updated_at`, `date`) VALUES
(47, 1, 'NS-001', NULL, 1, NULL, NULL, NULL, '2017-11-21 16:03:43', '2017-11-21 16:03:43', '2017-11-21'),
(48, 1, 'NS-002', NULL, 1, NULL, NULL, NULL, '2017-11-21 16:03:43', '2017-11-21 16:03:43', '2017-11-21'),
(49, 1, 'NS008', NULL, NULL, 1, NULL, NULL, '2017-11-21 16:03:43', '2017-11-21 16:03:43', '2017-11-21'),
(50, 1, 'NS009', 1, NULL, NULL, NULL, NULL, '2017-11-21 16:03:43', '2017-11-21 16:03:43', '2017-11-21'),
(79, 1, 'NS-001', NULL, NULL, NULL, 1, NULL, '2017-11-23 13:45:38', '2017-11-23 13:45:38', '2017-11-22'),
(80, 1, 'NS-002', NULL, NULL, NULL, 1, NULL, '2017-11-23 13:45:38', '2017-11-23 13:45:38', '2017-11-22'),
(81, 1, 'NS008', NULL, NULL, NULL, 1, NULL, '2017-11-23 13:45:38', '2017-11-23 13:45:38', '2017-11-22'),
(82, 1, 'NS009', NULL, NULL, NULL, 1, NULL, '2017-11-23 13:45:38', '2017-11-23 13:45:38', '2017-11-22'),
(83, 1, 'NS-001', NULL, 1, NULL, NULL, NULL, '2017-11-23 13:47:28', '2017-11-23 13:47:28', '2017-11-23'),
(84, 1, 'NS-002', NULL, NULL, 1, NULL, NULL, '2017-11-23 13:47:28', '2017-11-23 13:47:28', '2017-11-23'),
(85, 1, 'NS008', 1, NULL, NULL, NULL, NULL, '2017-11-23 13:47:28', '2017-11-23 13:47:28', '2017-11-23'),
(86, 1, 'NS009', 1, NULL, NULL, NULL, NULL, '2017-11-23 13:47:28', '2017-11-23 13:47:28', '2017-11-23'),
(91, 1, 'NS-001', 1, NULL, NULL, NULL, NULL, '2017-11-25 11:41:00', '2017-11-25 11:41:00', '2017-11-24'),
(92, 1, 'NS-002', 1, NULL, NULL, NULL, NULL, '2017-11-25 11:41:00', '2017-11-25 11:41:00', '2017-11-24'),
(93, 1, 'NS008', 1, NULL, NULL, NULL, NULL, '2017-11-25 11:41:00', '2017-11-25 11:41:00', '2017-11-24'),
(94, 1, 'NS009', 1, NULL, NULL, NULL, NULL, '2017-11-25 11:41:00', '2017-11-25 11:41:00', '2017-11-24'),
(103, 1, 'NS-001', 1, NULL, NULL, NULL, NULL, '2017-11-25 17:14:15', '2017-11-25 17:14:15', '2017-11-25'),
(104, 1, 'NS-002', 1, NULL, NULL, NULL, NULL, '2017-11-25 17:14:15', '2017-11-25 17:14:15', '2017-11-25'),
(105, 1, 'NS008', 1, NULL, NULL, NULL, NULL, '2017-11-25 17:14:15', '2017-11-25 17:14:15', '2017-11-25'),
(106, 1, 'NS009', NULL, NULL, 1, NULL, NULL, '2017-11-25 17:14:15', '2017-11-25 17:14:15', '2017-11-25'),
(107, 1, 'NS-001', NULL, NULL, NULL, 1, NULL, '2017-11-26 05:31:25', '2017-11-26 05:31:25', '2017-11-26'),
(108, 1, 'NS-002', NULL, 1, NULL, NULL, NULL, '2017-11-26 05:31:25', '2017-11-26 05:31:25', '2017-11-26'),
(109, 1, 'NS009', NULL, NULL, 1, NULL, NULL, '2017-11-26 05:31:25', '2017-11-26 05:31:25', '2017-11-26');

-- --------------------------------------------------------

--
-- Table structure for table `student_scores`
--

CREATE TABLE `student_scores` (
  `id` int(11) NOT NULL,
  `score_id` int(11) DEFAULT NULL,
  `lesson_group_id` int(11) DEFAULT NULL,
  `nis` varchar(50) DEFAULT NULL,
  `value` decimal(10,0) DEFAULT NULL,
  `assign_by` int(11) DEFAULT NULL,
  `exam_time` date DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_scores`
--

INSERT INTO `student_scores` (`id`, `score_id`, `lesson_group_id`, `nis`, `value`, `assign_by`, `exam_time`, `created_at`, `updated_at`) VALUES
(38, 1, 1, 'NS-002', '10', NULL, NULL, '2017-11-25 15:11:44', '2017-11-25 16:56:48'),
(39, 1, 1, 'NS-001', '23', NULL, NULL, '2017-11-25 16:56:30', '2017-11-25 16:56:30'),
(40, 2, 1, 'NS-001', '3', NULL, NULL, '2017-11-25 16:58:17', '2017-11-25 16:58:17'),
(41, 3, 1, 'NS-001', '67', NULL, NULL, '2017-11-25 16:59:51', '2017-11-25 16:59:51'),
(42, 4, 1, 'NS-001', '45', NULL, NULL, '2017-11-25 16:59:58', '2017-11-25 16:59:58'),
(43, 5, 1, 'NS-001', '90', NULL, NULL, '2017-11-25 17:00:10', '2017-11-25 17:00:10'),
(44, 3, 1, 'NS009', '56', NULL, NULL, '2017-11-25 17:00:18', '2017-11-25 17:00:18'),
(45, 1, 1, 'NS008', '11', NULL, NULL, '2017-11-25 17:03:33', '2017-11-25 17:03:33'),
(46, 1, 1, 'NS009', '11', NULL, NULL, '2017-11-25 17:03:33', '2017-11-25 17:03:33'),
(47, 5, 1, 'NS009', '60', NULL, NULL, '2017-11-25 17:14:31', '2017-11-25 17:14:31');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `address` text,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `degree` varchar(10) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `nip`, `birth_date`, `sex`, `address`, `phone`, `email`, `city`, `degree`, `lesson_id`, `password`, `status`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Toni', 'NK001', NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL),
(2, 'Vera', 'NK002', NULL, 2, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, NULL, NULL, NULL),
(3, 'Maya', 'NK003', NULL, 2, NULL, NULL, NULL, NULL, NULL, 3, NULL, 1, NULL, NULL, NULL),
(4, 'Sandi', 'NK004', NULL, 1, NULL, NULL, NULL, NULL, NULL, 4, NULL, 1, NULL, NULL, NULL),
(5, 'Hartono', 'NK005', NULL, NULL, NULL, NULL, 'teacher@scdocs.com', NULL, NULL, NULL, '$2y$10$SzW5abgXIImFh4558ZDLzudlLNmd76Or2yi.jbJ2zVJMCvmU6KYIe', NULL, '2017-11-18 15:28:53', '2017-11-18 15:28:53', 'gtWphXx7coHJ4Vi7JnuOPl9EfJXLLl4yRRqzRfWjgzjDX6vs2qaPq6jtqzhK');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_password_resets`
--

CREATE TABLE `teacher_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `active`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$k9hdShFzoaO7LTBVaN9pIeHMpAtSux/oqYviJH0IvFIaE5itks8my', 'GMDkaKgYsJl9BquK4cF7pEelQipJXKMfVHVGiOIgkFcICDaeD32UOwnHOOC3', '2017-11-15 07:02:51', '2017-11-15 07:02:51', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lesson_groups`
--
ALTER TABLE `lesson_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `student_attendances`
--
ALTER TABLE `student_attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_scores`
--
ALTER TABLE `student_scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_password_resets`
--
ALTER TABLE `teacher_password_resets`
  ADD KEY `teacher_password_resets_email_index` (`email`),
  ADD KEY `teacher_password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lessons`
--
ALTER TABLE `lessons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lesson_groups`
--
ALTER TABLE `lesson_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `student_attendances`
--
ALTER TABLE `student_attendances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `student_scores`
--
ALTER TABLE `student_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
